/*
    SPDX-FileCopyrightText: 2020, 2021 Igor Kushnir <igorkuo@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <regex>
#include <string>
#include <string_view>
#include <utility>
#include <vector>

template <typename GetMessage>
void doAssert(bool condition, GetMessage getMessage)
{
    if (condition)
        return;
    std::cerr << "** QBENCHMARK_output_parser FATAL ** " << getMessage() << '\n';
    std::abort();
}

void doAssert(bool condition, const char * message)
{
    doAssert(condition, [message] { return message; });
}

void doAssertNoFail(const std::string & prefix, const std::string & line)
{
    static constexpr std::string_view failPrefix = "FAIL!";
    doAssert(line.size() < prefix.size() + failPrefix.size()
             || !std::equal(prefix.cbegin(), prefix.cend(), line.cbegin())
             || !std::equal(failPrefix.cbegin(), failPrefix.cend(),
                            line.cbegin() + prefix.size()),
             [&line] { return "A test case failed: \"" + line + "\"."; });
}

constexpr auto optimizeFlags = std::regex::ECMAScript | std::regex::optimize;

class BenchmarkResults
{
public:
    explicit BenchmarkResults(const std::string & prefix,
                              const std::string & benchmarkName)
        : resultRegex_(prefix + "RESULT : " + benchmarkName + "(.*):",
                       optimizeFlags),
          msPerIterationRegex_(prefix + R"(     ([\d,]+(?:\.\d+)?) )"
                + msPerIterationString + R"( \(total: [\d,]+, iterations: \d+\))",
                               optimizeFlags)
    {}

    void parseLine(const std::string & line)
    {
        if (expectedResult_ == results_.size()) {
            std::smatch match;
            if (!std::regex_match(line, match, resultRegex_))
                return;
            std::string testCaseName = match.str(1);

            const auto it = std::find_if(results_.cbegin(), results_.cend(),
                        [&testCaseName](const Result & r) {
                            return testCaseName == r.testCaseName;
                        });
            if (it == results_.cend()) {
                results_.emplace_back(std::move(testCaseName));
                expectedResult_ = results_.size() - 1;
            } else
                expectedResult_ = it - results_.cbegin();
        } else {
            std::smatch match;
            const bool matched = std::regex_match(line, match,
                                                  msPerIterationRegex_);
            doAssert(matched, [&line] {
                return std::string{"Expected \""} + msPerIterationString
                        + "\", but found: \"" + line + "\".";
            });
            results_[expectedResult_].msPerIteration.emplace_back(match.str(1));
            expectedResult_ = results_.size();
        }
    }

    void endTestRun() const {
        doAssert(expectedResult_ == results_.size(), [] {
            return std::string{"Expected \""} + msPerIterationString
                    + "\", but found a test run end.";
        });
    }

    void print(bool sortNumbers) {
        for (auto & r : results_) {
            std::cout << r.testCaseName << '\n';
            if (sortNumbers)
                std::sort(r.msPerIteration.begin(), r.msPerIteration.end());
            for (const auto & ms : r.msPerIteration)
                std::cout << '\t' << ms.str << '\n';
            std::cout << '\n';
        }
    }

private:
    static constexpr const char * msPerIterationString{"msecs per iteration"};

    std::regex resultRegex_;
    std::regex msPerIterationRegex_;

    struct Result {
        explicit Result(std::string testCaseName)
            : testCaseName{std::move(testCaseName)}
        {}

        struct Ms {
            long double value;
            std::string str;

            explicit Ms(std::string str)
                : value{valueFromStr(str)}, str{std::move(str)}
            {}

            friend bool operator<(const Ms & a, const Ms & b) {
                return a.value < b.value;
            }

        private:
            static double valueFromStr(std::string str)
            {
                // Qt Test uses commas as integer digit separators => remove them
                // before passing the string to the standard conversion function.
                str.erase(std::remove(str.begin(), str.end(), ','), str.end());
                return stold(str);
            }
        };

        std::string testCaseName;
        std::vector<Ms> msPerIteration;
    };
    std::vector<Result> results_;
    std::size_t expectedResult_{0};
};

class MultiBenchmarkResults
{
public:
    BenchmarkResults & benchmarkResults(const std::string & prefix,
                                        const std::string & benchmarkName)
    {
        const auto it = std::find_if(results_.begin(), results_.end(),
                    [&benchmarkName](const Result & r) {
                        return benchmarkName == r.benchmarkName;
                    });
        if (it != results_.end())
            return it->results;
        results_.emplace_back(prefix, benchmarkName);
        return results_.back().results;
    }

    void print(std::string_view asterisks, bool sortNumbers) {
        for (auto & [name, result] : results_) {
            std::cout << asterisks << ' ' << name << ' ' << asterisks << '\n';
            result.print(sortNumbers);
            std::cout << '\n';
        }
    }

private:
    struct Result {
        explicit Result(const std::string & prefix,
                        const std::string & benchmarkName)
            : benchmarkName{benchmarkName}, results(prefix, benchmarkName)
        {}

        std::string benchmarkName;
        BenchmarkResults results;
    };
    std::vector<Result> results_;
};

class Asterisks
{
public:
    static auto createEscaped() {
        std::string result(asteriskCount * 2, 0);
        for (std::size_t i = 0; i < result.size(); ++i) {
            result[i] = '\\';
            result[++i] = '*';
        }
        return result;
    }

    static auto create() {
        return std::string(asteriskCount, '*');
    }

private:
    static constexpr std::size_t asteriskCount{9};
};

void run(const bool sortNumbers)
{
    const auto escapedAsterisks = Asterisks::createEscaped();
    const std::regex startRegex(
            "(\\d+: )?" + escapedAsterisks + " Start testing of (\\w+) "
            + escapedAsterisks,
            optimizeFlags);
    const auto asterisks = Asterisks::create();
    MultiBenchmarkResults allResults;

    std::string line;
    while (getline(std::cin, line)) {
        std::smatch startMatch;
        if (!std::regex_match(line, startMatch, startRegex))
            continue;
        const auto prefix = startMatch.str(1);
        const auto benchmarkName = startMatch.str(2);
        BenchmarkResults & results = allResults.benchmarkResults(prefix,
                                                                 benchmarkName);
        const auto endIndicator = prefix + asterisks
                                  + " Finished testing of " + benchmarkName
                                  + ' ' + asterisks;

        while (getline(std::cin, line) && line != endIndicator) {
            doAssertNoFail(prefix, line);
            doAssert(!std::regex_match(line, startRegex), [&] {
                return "A new test started before the previous one finished. "
                       "Expected: \"" + endIndicator + "\". Found: \"" + line
                       + "\".";
            });
            results.parseLine(line);
        }
        results.endTestRun();
    }
    allResults.print(asterisks, sortNumbers);
}

int main(int argc, char * argv[])
{
#ifdef REDIRECT_STDIO_TO_FILES
    freopen("../input", "r", stdin);
    freopen("../output", "w", stdout);
#endif
    std::ios_base::sync_with_stdio(false);

    const bool sortNumbers = argc > 1 && std::strcmp(argv[1], "sort") == 0;
    run(sortNumbers);
}
